//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function block=WRITE_CSV_sim(block,flag)
  global data_to_write;
    
  select flag
     case -5 // Error

     case 0 // Derivative State Update


     case 1 // Output Update
      // FIXME: Mettre ici tout ce qui sert a lire sur le port serie

      //y1 = PIC_receive();
      //block.outptr(1) = y1;

      // Pour tester
      data=ones(1,1+block.rpar(1))
      //data=[scicos_time(),block.inptr]
      //disp(data)
      data(1)=scicos_time();
      for i=1:block.rpar(1)
          data(i+1)=block.inptr(i);
      end
      data_to_write($+1,:)=data;

     case 2 // State Update

     case 3 // OutputEventTiming

     case 4 // Initialization
        data_to_write=[];
        
     case 5 // Ending
     // FIXME: quoi faire a la fin de la simulation
        path=ascii(block.ipar);
        delimiter=ascii(block.rpar(2));
        if delimiter=="s" then
            delimiter=" ";
        end
        csvWrite(data_to_write,path,delimiter)

     case 6 // Re-Initialisation

     case 9 // ZeroCrossing

    else // Unknown flag

    end
endfunction
