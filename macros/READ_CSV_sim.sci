//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function block=READ_CSV_sim(block,flag)
  global data_read_csv;
    
  select flag
     case -5 // Error

     case 0 // Derivative State Update


     case 1 // Output Update
      // FIXME: Mettre ici tout ce qui sert a lire sur le port serie

      for i=1:block.rpar(1)
          if scicos_time()>data_read_csv($,1) then
              block.outptr(i)=data_read_csv($,i+1)
          else
              block.outptr(i) = interpln(data_read_csv(:,[1,i+1])',scicos_time()) ;
          end
      end
      

     case 2 // State Update

     case 3 // OutputEventTiming

     case 4 // Initialization
        path=ascii(block.ipar);
        delimiter=ascii(block.rpar(2));
        if delimiter=="t" then
            delimiter="\t";
        end
        data_read_csv=read_csv(path,delimiter);
        data_read_csv=data_read_csv(block.rpar(3)+1:$,:);
        data_read_csv=evstr(data_read_csv);
        
     case 5 // Ending
     // FIXME: quoi faire a la fin de la simulation

     // PIC_put_zero();

     case 6 // Re-Initialisation

     case 9 // ZeroCrossing

    else // Unknown flag

    end
endfunction
