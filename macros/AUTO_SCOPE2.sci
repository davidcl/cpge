//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=AUTO_SCOPE2(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
     case 'plot' then
// deprecated
     case 'getinputs' then
// deprecater
     case 'getoutputs' then
// deprecated
     case 'getorigin' then
// deprecated
     case 'set' then
         x=arg1;
         graphics=arg1.graphics;
         exprs=graphics.exprs
         model=arg1.model;
         while %t do
            [ok,num_points,tf,name,exprs]=scicos_getvalue('Scope parameters',..
                                                        ['Number of points';'End simulation time'; 'Scope Name (optional)'], ..
                                                        list('vec',1,'vec',1,'str','-1'), ..
                                                        exprs)
          mess=[];
          if ~ok then //cancel
              break;
          end

          if num_points <= 0
              mess=[mess ;_("Number of points must be positive.")]
              ok = %f
          end
          
          if ok then
              num_inputs=2;
              model.rpar.objs(1).graphics.exprs=[string(tf)+"/"+string(num_points) ; "0"]
              model.rpar.objs(1).model.rpar=[tf/num_points ; 0]
              tf_vec=tf*ones(num_inputs,1);
              model.rpar.objs(2).graphics.exprs(8) = strcat(string(tf_vec),' ');
              model.rpar.objs(2).graphics.exprs(11) = name;
              model.rpar.objs(2).model.rpar(2:num_inputs+1) = tf_vec;
              model.rpar.objs(3).graphics.exprs = string(tf);
              model.rpar.objs(3).model.firing = tf;
              graphics.exprs = exprs;
              x.model=model;
              x.graphics = graphics;
              break
          else
              message(mess);
          end
         end
     case 'define' then


      tf = 10;
      num_points = 200;
      num_inputs = 2;
      
      //definition of parameters for CMSCOPE
      tf_vec=tf*ones(num_inputs,1);
      rpar=[0;tf_vec];
      for i=1:num_inputs
          rpar($+1)=-10;
          rpar($+1)=10;
      end
   
      //definition of the block CMSCOPE
      scope=CMSCOPE('define');
      scope.graphics.exprs(1) = strcat(string(ones(num_inputs,1)),' ')
      scope.graphics.exprs(6) = strcat(string(-10*ones(num_inputs,1)),' ')
      scope.graphics.exprs(7) = strcat(string(10*ones(num_inputs,1)),' ')
      scope.graphics.exprs(8) = strcat(string(tf_vec),' ')
      scope.model.in=ones(num_inputs,1);
      scope.model.in2=ones(num_inputs,1);
      scope.model.intyp=ones(num_inputs,1);
      scope.model.rpar=rpar;
      //connection to links number
      scope.graphics.pin = 3+num_inputs+[1:num_inputs]';
      scope.graphics.pein = 3+2*num_inputs;
      scope.graphics.in_implicit=strsubst(string(ones(num_inputs,1)),"1","E");

      clockc=SampleCLK('define')
      clockc.graphics.peout=4+2*num_inputs;
      clockc.graphics.exprs=[string(tf)+"/"+string(num_points) ; "0"]
      clockc.model.rpar = [tf/num_points ; 0] 

      //definition of the inputs blocks    
      input_port=list();
      for i=1:num_inputs
          input_port(i)=IN_f('define')
          input_port(i).graphics.exprs=[string(i)]
          input_port(i).model.ipar=[i];
          input_port(i).graphics.pout=3+num_inputs+i;  
      end      

      endBlock=END_c('define');
      endBlock.graphics.exprs = string(tf);
      endBlock.model.firing = tf;
      endBlock.graphics.pein=4+2*num_inputs+1;
      endBlock.graphics.peout=4+2*num_inputs+1;

      diagram=scicos_diagram();
      diagram.objs(1)=clockc;      
      diagram.objs(2)=scope;
      diagram.objs(3)=endBlock;      
      for i=1:num_inputs
          diagram.objs($+1)=input_port(i);
      end
      for i=1:num_inputs
          diagram.objs($+1)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1, 1], from=[3+i, 1, 0], to=[2, i, 1])
      end
      diagram.objs($+1)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[5, -1], from=[1, 1, 0], to=[2, 1, 1])
      diagram.objs($+1)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[5, -1], from=[3, 1, 0], to=[3, 1, 1])

      model = scicos_model();
      model.sim='csuper'
      model.in=[1;1]
      model.in2=[1;1]
      model.intyp=[1;1]
      model.blocktype='h'
      model.dep_ut=[%f %f]
      model.rpar=diagram
      x = standard_define([2 2], model, "", [])
      x.gui='AUTO_SCOPE2'
      x.graphics.exprs=[string(num_points) ; string(tf) ; ""]
    end
endfunction

