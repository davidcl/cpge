//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x, y, typ]=XCOS_BUTTON(job, arg1, arg2)
    x=[];
    y=[];
    typ=[];
    select job
     case 'plot' then
// deprecated
     case 'getinputs' then
// deprecater
     case 'getoutputs' then
// deprecated
     case 'getorigin' then
// deprecated
     case 'set' then
        x=arg1;
        graphics=arg1.graphics;
        exprs=graphics.exprs
        model=arg1.model;

        // Boite de dialogue pour indiquer le nom du bouton et les valeurs renvoyées sur on et sur off
        while %t do
            [ok,name,valOn,valOff, exprs]=scicos_getvalue('Paramètres du bouton',..
                                                ['Nom';'Valeur renvoyée sur On';'Valeur renvoyée sur Off'], ..
                                                list('str',-1,'vec',-1,'vec',-1),exprs);

            mess=[];
            if ~ok then
              break;
            end

            if ok then
              rep=1
              model.rpar=[valOn,valOff,rep];
              model.opar=list(name)
              graphics.exprs=exprs;
              x.model=model;
              x.graphics=graphics;
              break;
           end
        end
        
     case 'define' then
      model=scicos_model();
      model.sim=list("XCOS_BUTTON_sim",5)
      model.blocktype='c';
      model.dep_ut=[%f %t];
      model.out=[1];
      model.evtin=[1];
      model.firing=[0;-1]
      name='bouton'
      valOn=1   //valeur quand il est appuyé
      valOff=0 //valeur quand le bouton n'est pas appuyé
      rep=1 //repérage du bouton dans la liste des boutons (modifié dans pre_xcos_simulate)
      model.rpar=[valOn,valOff,rep];
      model.opar=list(name)
      x=standard_define([2 2],model,[],[]);
      x.graphics.in_implicit=[];
      x.graphics.out_implicit=['E'];
      x.graphics.exprs=[name,string(valOn),string(valOff)];
    end
endfunction
