//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010-2012 - Scilab Enterprises - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function continueSimulation=Param_Temp_pre_simulate(scs_m, needcompile)

    //On crée un nouveau fichier scs pour ne pas écraser le premier
    scs=scs_m
    // Retrieve all objects
    objects = scs_m.objs;

    nombre_blocs=0;    //Nombre de blocs dans le diagramme
    nombre_liens=0;    //Nombre de lien dans le diagramme

    param1="";
    param1val=[];
    param2="";
    param2val=[];
    param3="";
    param3val=[];

    todemux = %f;

    continueSimulation = %f;

    //Récupère le nombre de blocs dans le modèle
    for i=1:size(scs.objs)
        if typeof(scs.objs(i))=='Block' then
            nombre_blocs=nombre_blocs+1;
        end
    end
    nombre_liens=size(scs.objs)-nombre_blocs;  //calcul du nombre de liens

    nb_scopes=0 //Nombre de scope trouvé
    nom_scope=list();
    num_scope=[];    //numéro du bloc correspondant à chaque SCOPE
    nb_curves_by_scope=[]; //nombre de courbes à tracer sur un même SCOPE
    nb_total_curves=0;    //nombre total de courbes à tracer
    legendes=cell();
    // On récupere le numéro du bloc PARAM_VAR puis les caractéristiques correspondantes
    // On récupère le bloc REP_TEMP pour les paramètres temporels

    for i = 1:size(scs.objs)
        curObj= scs.objs(i);
        if (typeof(curObj) == "Block" & curObj.gui == "PARAM_VAR")
            if(length(curObj.model.opar)==0) then
                message(['Double-cliquer sur le bloc PARAM. variation pour mettre à jour les paramètres'])
                return;
            end
            if length(curObj.model.opar)>0 then
                param1=curObj.model.opar(1)
                param1val=curObj.model.opar(2)
            end
            if length(curObj.model.opar)>2 then
                param2=curObj.model.opar(3)
                param2val=curObj.model.opar(4)
            end
            if length(curObj.model.opar)>4 then
                param3=curObj.model.opar(5)
                param3val=curObj.model.opar(6)
            end
        elseif (typeof(curObj) == "Block" & curObj.gui == "REP_TEMP")
            nb_pts_db=curObj.model.rpar(1);
            nb_pts_st=string(nb_pts_db)
            temps_st=curObj.graphics.exprs(1);
            temps_db=evstr(temps_st);
            grid_on=evstr(curObj.graphics.exprs(2));
            scs.props.tf=temps_db;
            rep_temp=1;
        end
    end

    // On liste le nombre de blocs SCOPE et on modifie les noms des variables des SCOPE ainsi que les paramètres
    for i = 1:size(scs.objs)
        if (typeof(scs.objs(i)) == "Block" & scs.objs(i).gui == "SCOPE" )
            nb_scopes=nb_scopes+1;
            num_scope(nb_scopes)=i;
            nb_curves_by_scope(nb_scopes)=evstr(scs.objs(i).graphics.exprs(1));

            //recherche parmi les blocs la clock et le tows
            list_obj=scs.objs(i).model.rpar.objs;
            no=1;
            for j=1:size(list_obj)
                if (typeof(list_obj(j)) == "Block" & list_obj(j).gui == "TOWS_c") then //on affecte un nom pour le stockage dans scilab
                    scs.objs(i).model.rpar.objs(j).graphics.exprs=[nb_pts_st;"o"+string(no+nb_total_curves);"0"];
                    scs.objs(i).model.rpar.objs(j).model.ipar=[nb_pts_db;2;24;no+nb_total_curves];
                    no=no+1;
                elseif (typeof(list_obj(j)) == "Block" & list_obj(j).gui == "SampleCLK") then //on modifie le pas de temps
                    scs.objs(i).model.rpar.objs(j).graphics.exprs=[string(temps_db/nb_pts_db);"0"];
                    scs.objs(i).model.rpar.objs(j).model.rpar=[temps_db/nb_pts_db;0];  
                end
            end

            legendes{nb_scopes}=scs.objs(i).graphics.exprs(2:$);
            nb_total_curves=nb_total_curves+nb_curves_by_scope(nb_scopes);
        end
    end

    //recherche du nombre de paramètres variables
    num_param=0;
    if ~isempty(param1) then num_param=num_param+1;
    end
    if ~isempty(param2) then num_param=num_param+1;
    end
    if ~isempty(param3) then num_param=num_param+1;
    end



    //Lancement d'une barre de progression
    x=0

    winId=waitbar('Simulation in progress')

    D=[]
    //handle_fig=figure();
    cont=[]
    z=[]


    //    for z=1:nombre_scope
    //        handle_fig(z)=figure();
    //    end
    //    legend_c=[]

    //toutes les courbes sont réunies sur la même figure, une sous-figure par SCOPE, sur chaque sous-figure, autant de courbes que de simulations et d'entrées d'un bloc scope
    handle_fig=figure();
    set(handle_fig,"background",8)
    legend_by_simul=[]
    drawlater()
    //c_color=[[0.75,0.75,0];[0.25,0.25,0.25];[0,0,1];[0,0.5,0];[1,0,0];[0,0.75,0.75];[0.75,0,0.75]];
    plot_properties.c_color=['m','b','r','k','g','c'];
    plot_properties.m_type=['-','--',':','-.'];
    plot_properties.legendes=legendes;
    plot_properties.nb_scopes=nb_scopes;
    plot_properties.nb_curves_by_scope=nb_curves_by_scope;
    //Lancement des simulations
    Nsimu=0  //Numero de la simu
    legend_by_subplot=[];
    scopes_legend=cell(nb_scopes,1);

    select num_param
    case 1 //1 parametre

        for i=1:length(param1val)
            //mise à jour de la barre de progression
            x=(i-1)/length(param1val)
            waitbar(x,winId)
            //modification du contexte et simulation
            execstr("cont"+"."+param1+"="+string(param1val(i)))
            scicos_simulate(scs, cont);
            //stockage du nom de la simulation avec la valeur du parametre
            Nsimu=Nsimu+1;
            legend_by_simul(Nsimu)=string(param1)+'='+string(param1val(i))
            plot_on_scope(scopes_legend,plot_properties,legend_by_simul,scs)

        end     

        delete(winId);

    case 2 //2 parametres

        for i=1:length(param1val)
            execstr("cont"+"."+param1+"="+string(param1val(i)));
            for j=1:length(param2val)
                x=((i-1)*length(param2val)+(j-1))/(length(param1val)*length(param2val));
                waitbar(x,winId);
                execstr("cont"+"."+param2+"="+string(param2val(j)));
                Nsimu=Nsimu+1;
                scicos_simulate(scs,cont);
                //stockage du nom de la simulation avec la valeur du parametre
                legend_by_simul(Nsimu)=string(param1)+'='+string(param1val(i))+', '+string(param2)+'='+string(param2val(j));
                plot_on_scope(scopes_legend,plot_properties,legend_by_simul,scs)

            end
        end  

        delete(winId);

    case 3 // trois parametres variables
        for i=1:length(param1val)
            execstr("cont"+"."+param1+"="+string(param1val(i)));
            for j=1:length(param2val)
                execstr("cont"+"."+param2+"="+string(param2val(j)));
                for k=1:length(param3val)
                    x=((i-1)*length(param2val)*length(param3val)+(j-1)*length(param3val)+(k-1))/(length(param1val)*length(param2val)*length(param3val));
                    waitbar(x,winId);
                    execstr("cont"+"."+param3+"="+string(param3val(k)));
                    Nsimu=Nsimu+1
                    scicos_simulate(scs,cont);
                    legend_by_simul(Nsimu)=string(param1)+'='+string(param1val(i))+', '+string(param2)+'='+string(param2val(j))+', '+string(param3)+'='+string(param3val(k));

                    plot_on_scope(scopes_legend,plot_properties,legend_by_simul,scs)

                end
            end
        end
        delete(winId);
    end
    
	set(handle_fig,"background",8)

	if grid_on==1 then
		xgrid
	end       
    
    //nicescope();
    drawnow()
endfunction

function plot_on_scope(scopes_legend,plot_properties,legend_by_simul,scs)
    c_color=plot_properties.c_color;
    m_type=plot_properties.m_type;
    legendes=plot_properties.legendes;
    nb_scopes=plot_properties.nb_scopes;
    nb_curves_by_scope=plot_properties.nb_curves_by_scope;   
    legend_by_subplot=[];
    //scopes_legend=cell(nb_scopes);
    for l=1:nb_scopes
        subplot(nb_scopes,1,l);
        legend_by_subplot=legendes{l};
        if size(legend_by_subplot,1)~=nb_curves_by_scope(l) then
            legend_by_subplot="courbe"+string([1:nb_curves_by_scope(l)]);
        end
        scopes_legend{l}=[scopes_legend{l} legend_by_simul(Nsimu)+" "+legend_by_subplot];

        list_obj=scs.objs(num_scope(l)).model.rpar.objs;
        no=1;
        for m=1:size(list_obj)
            if (typeof(list_obj(m)) == "Block" & list_obj(m).gui == "TOWS_c") then
                label=list_obj(m).graphics.exprs(2);
                D=evstr(label);
                plot(D.time,D.values,c_color(modulo(Nsimu,6)+1)+m_type(modulo(no,4)),'thickness',2)
                no=no+1;    
                e=gce()
                e.children($).display_function = "formatTempTip";
                //ok=datatipInitStruct(e.children($),"formatfunction","formatTempTip")                       
            end
        end         
//        for i=1:size(ax.children,1)
//                  disp(ax.children(i).children($))
//                  if ax.children(i).children.type ~='legend' then
//                  ok=datatipInitStruct(ax.children(i).children($),"formatfunction","formatTempTip")
//                  end
//              end  

        //if l==nb_scopes
            //set(handle_fig,"background",8)
            h=legend(scopes_legend{l});
//            //h.background=-2;
            set(h,"background",8)
            set(h,"legend_location","upper_caption")
//            if grid_on==1 then
                xgrid
//            end
        //end
    end     
    scopes_legend=resume(scopes_legend)
endfunction

function str=formatTempTip(curve,pt,index)
//this function is called by the datatips mechanism to format the tip
//string for the magnitude bode curves
  str=msprintf("%.4g"+_("s")+"\n%.4g", pt(1),pt(2))
endfunction

