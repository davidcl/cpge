//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
function PIC_put_zero()
    global connected port_TCL
    if (connected == 1) then
        u1=0;
        values= "P" + ascii((int(evstr(u1)/256))) + ascii(modulo(evstr(u1),256))
        writeserial(port_TCL,values);
        [q,flags]=serialstatus(port_TCL)
        while(q<8)
            [q,flags]=serialstatus(port_TCL)
        end
        values=readserial(port_TCL);

    end
endfunction
