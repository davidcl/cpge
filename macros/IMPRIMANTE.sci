//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=IMPRIMANTE(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
     case 'plot' then
// deprecated
     case 'getinputs' then
// deprecater
     case 'getoutputs' then
// deprecated
     case 'getorigin' then
// deprecated
     case 'set' then
      x=arg1;
      graphics=arg1.graphics;
      exprs=graphics.exprs
      model=arg1.model;
      while %t do
      [ok,environnement,port_TCL,exprs]=scicos_getvalue('Scope parameters',..
                                                        ['Environnement ( 1 : windows / 0 : linux)';'Port Com'],..
                                                        list('vec',1,'vec',1), ..
                                                        exprs);
          mess=[];

          if ~ok then // Cancel
              break;
          end


          if environnement ~= 1 & environnement ~= 0
              mess=[mess ; "Taper 1 pour windows ou 0 pour linux"]
              ok = %f
          end
           
          if ok then
              graphics.exprs = exprs;
              model.rpar=[environnement;port_TCL];
              x.model=model;
              x.graphics = graphics;
              break;
          else
              message(mess);
          end
      
      
      end
     case 'define' then
       model=scicos_model()
        model.sim=list("IMPRIMANTE2_sim", 5)
        model.blocktype='d';
        model.dep_ut=[%f %f];
        model.in=[1];
        model.evtin=[1]
        model.firing=[0;-1]
        model.intyp=[1];
        model.out=[1];
        model.rpar=[1;6]; //windows par defaut et com 6
        x=standard_define([6 6],model,[],[]);
        x.graphics.in_implicit=['E'];
        x.graphics.out_implicit=['E'];
        x.gui="IMPRIMANTE";
        x.graphics.exprs=[string(1) ;string(6)]
    end
    
endfunction

