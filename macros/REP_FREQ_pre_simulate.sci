//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010-2010 - DIGITEO -
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//


function continueSimulation=REP_FREQ_pre_simulate(scs_m, needcompile)
    scs=[]

    // On recopie le scs_m
    scs=scs_m;
    // Retrieve all objects
    objs = scs_m.objs;

    nombre_blocs=0;    //Nombre de blocs dans le diagramme
    nombre_liens=0;    //Nombre de lien dans le diagramme
    nombre_rep_freq=0; //Nombre de blocs REP_FREQ dans le diagramme
    nombre_grandeur_phy=0;  //Nombre de blocs IN_Bode dans le diagramme
    num_grandeur_phy=[];    //Numéro des blocs grandeur physique dans le diagramme
    input_signals=[];   //vecteur des grandeurs d'entrée
    output_signals=[];   // vecteur des grandeurs de sortie
    type_diag=[];        // vecteur des types de diagrammes à tracer
    //    nombre_out_bode=0;  //Nombre de blocs OUT_Bode dans le diagramme
    //    num_out_bode=[];    //Numéro des blocs OUT_bode dans le diagramme
    w_min=[];
    w_max=[];
    num_w=[];            // Nombre de fréquences à tracer
    omega=[]             //vecteur des pulsations à tracer dans la réponse fréquentielle
    marges=[];           //marges à ne pas tracer
    asymp=[];            //asymptote

    //Récupère le nombre de blocs dans le modèle
    for i=1:size(objs)
        if typeof(objs(i))=='Block' then
            nombre_blocs=nombre_blocs+1;
        end
    end
    nombre_liens=size(objs)-nombre_blocs;  //calcul du nombre de liens

    // Récupère les blocs REP_freq et les paramètres associés
    for i=1:nombre_blocs
        if objs(i).gui=="REP_FREQ" then nombre_rep_freq=nombre_rep_freq+1;
            num_rep_freq(nombre_rep_freq)=i;
            input_signals(nombre_rep_freq)=objs(i).model.opar(1);
            output_signals(nombre_rep_freq)=objs(i).model.opar(2);
            type_diag(nombre_rep_freq)=objs(i).model.ipar;
            w_min(nombre_rep_freq)=objs(i).model.rpar(1);
            w_max(nombre_rep_freq)=objs(i).model.rpar(2);
            num_w(nombre_rep_freq)=objs(i).model.rpar(3);
            marges(nombre_rep_freq)=objs(i).model.rpar(4);
            asymp(nombre_rep_freq)=objs(i).model.rpar(5);
            //option pour trace de la symetrie dans Nyquist
            if size(objs(i).model.rpar,2)==6 then
                if objs(i).model.rpar(6)==1 then
                    sym(nombre_rep_freq)=%t
                else
                    sym(nombre_rep_freq)=%f
                end
            else
                sym(nombre_rep_freq)=%f
            end
            
        end
        if objs(i).gui=="PID" then 
            message(["Le bloc PID est pour l''instant imcompatible avec le bloc BODE. Utiliser le bloc PIDfiltered"])
            erreur1=%t           
            return
        end
    end

    // s'il y a déja des blocs IN_f et OUT_f, on demande à l'utilisateur de les retirer
    probleme_in_f=%f
    for i=1:nombre_blocs
        if objs(i).gui=="IN_f" | objs(i).gui=="OUT_f" then probleme_in_f=%t;
        end
    end
    if probleme_in_f then disp('Le tracé des réponses fréquentielles est incompatible avec la présence de blocs IN_f et OUT_f')
        return
    end

    //-------------------------- Traitement à proprement parler-------------------------------
    // Pour chacun des bloc REP_FREQ trouvés dans le diagramme on extrait la FT
    for i=1:nombre_rep_freq   //i est le bloc rep_freq que l'on traite

        //test si on doit superposer plusieurs graphes sur une meme figure
        inputs=stripblanks(strsplit(input_signals(i),';'))
        outputs=stripblanks(strsplit(output_signals(i),';'))
        nb_inputs=size(inputs,1)
        if size(inputs,1)<>size(outputs,1) then
            messagebox(['Le nombre de grandeurs physiques d''entrée ne correspond pas au nombre de grandeurs physiques de sortie';
                        string(inputs)+" en entrées";
                        string(outputs)+" en sorties"])
            erreur1=%t;
            continueSimulation = %t
            return
        end

        sys=list()
        tf=list()
        total_delay_BO=zeros(nb_inputs,1)
        for j=1:nb_inputs

            presence_delay=%f; //flag indiquant si le schéma contient au moins un bloc TIME_DELAY
            delay_parameters=[]; //valeur des TIME_DELAY 
            num_delay=[]; //stockage des numéros de blocs TIME_DELAY
            BO_delay=[]; //flag indiquant si le bloc est dans la BO ou dans une BF        
            erreur1=%f;

            //modification du schema-bloc
           
            [scs,presence_delay,param_delays,err]=create_new_block_diag(scs_m,inputs(j),outputs(j))
            if err==1 then
                disp('erreur dans create_new_block_diag')
                return
            end
            
            num_delay=param_delays.num_delay;
            delay_parameters=param_delays.delay_parameters;
            BO_delay=param_delays.BO_delay;

            //evaluation du contexte
            context=scs.props.context;
            [%scicos_context, ierr] = script2var(context,struct())
            %cpr=list();
            //   if exists('needcompile') then
            [scs, %cpr, needcompile, ok] = do_eval(scs, %cpr,%scicos_context);

            // On linearise le modele
            try
                sys(j)=lincos(scs);
                tf(j)=ss2tf(sys(j));  
                tf(j)=clean(tf(j))
                if presence_delay then
                   //[sys(j),tf(j),total_delay_BO(j)]=extract_tf_with_delay(scs,num_delay,BO_delay,total_delay_BO(j),delay_parameters)
                   //test pour savoir si le bloc retard est dans une BO ou BF
                   // long car on linéarise en remplaçant le bloc par 100 puis par 100
                    for l=1:size(num_delay,1)
                        num_obj=num_delay(l);
                        tr=delay_parameters(l);

                        scs2=scs;
                        gain=GAINBLK_f('define')
                        gain.graphics.pin = scs2.objs(num_obj).graphics.pin;
                        gain.graphics.pout = scs2.objs(num_obj).graphics.pout;
                        gain.model.rpar=100;
                        gain.graphics.exprs="100";
                        scs2.objs(num_obj)=gain;
                        sys2=lincos(scs2);
                        tf2=ss2tf(sys2);
                        tf2=clean(tf2)

//                        gain.model.rpar=100;
//                        gain.graphics.exprs="100";
//                        scs2.objs(num_obj)=gain;
//                        sys3=lincos(scs2);
//                        tf3=ss2tf(sys3);
//                        test_bo=tf3/tf2;

                       test_bo=tf2/tf(j);
                        [n,d]=simp(test_bo.num,test_bo.den)
                        test_bo=n/d
                        if stripblanks(strcat(string(test_bo.num)))=='100' then
                            disp("Bloc dans une BO - utilisation de iodelay") //on laisse le bloc avec un gain de 1, on multipliera la BO par exp(-tr*s) ensuite
                            BO_delay(find(num_delay==num_obj))=1;
                            total_delay_BO(j)=total_delay_BO(j)+tr;
                            
                        elseif stripblanks(strcat(string(test_bo.num)))=='1' | (stripblanks(strcat(string(test_bo.num)))==stripblanks(strcat(string(test_bo.den)))) then
                            disp("Bloc retard hors du schéma-bloc étudié")
                            BO_delay(find(num_delay==num_obj))=-1;
                            
                        else
                            disp("Bloc dans une BF - Approximation Pade") //on modifie le bloc en le remplaçant par une approximation de Pade à l'ordre 3
                            BO_delay(find(num_delay==num_obj))=0;
                        end
                    end

                    //nouvelle boucle pour mettre à jour le schéma bloc de manière à prendre en compte les approximations de Pade ou non
                    for l=1:size(num_delay,2)
                        num_obj=num_delay(l);
                        tr=delay_parameters(l);
                        if BO_delay(l)==0 then
                            s=poly(0,"s");

                            texte=['Un bloc retard est situé dans une boucle fermée.'
                            'Il est remplacé par une approximation de Padé (ordre >=1) à choisir.'
                            'Attention, l''approximation est valable pour des fréquences inférieures à 1/retard environ']
                            ordre_pade=x_mdialog(texte,'Ordre approximation','8')
                            if ordre_pade==[] then
                                disp('Ordre mis à 8 par défaut')
                                ordre_pade=8
                            end
                            ordre_pade=evstr(ordre_pade)
                            ex=-tr*s
                            [num,den]=pade(ex,ordre_pade,ordre_pade)
                            
                            scs.objs(num_obj).graphics.exprs=[sci2exp(num),sci2exp(den)];
                            
                            //on doit pouvoir faire plus simple pour avoir rpar...
                            H=cont_frm(num,den);
                            [A,B,C,D]=H(2:5);
                            [ns1,ns1]=size(A);
                            rpar=[matrix(A,ns1*ns1,1);
                            matrix(B,ns1,1);
                            matrix(C,ns1,1);
                            D]
                            
                            scs.objs(num_obj).model.rpar=rpar;
                        end    
                    end
//                    
                    //evaluation du contexte pour avoir la bonne FT
                    context=scs.props.context;
                    [%scicos_context, ierr] = script2var(context,struct())
                    %cpr=list();
                    //   if exists('needcompile') then
                    [scs, %cpr, needcompile, ok] = do_eval(scs, %cpr,%scicos_context);

                    sys(j)=lincos(scs)
                    tf(j)=ss2tf(sys(j)) 
                    tf(j)=clean(tf(j))
         
                end
            // On indique la fonction de transfert dans la console :
                disp(["Scilab a réussi à extraire une fonction de transfert du schéma bloc : ("+sci2exp(tf(j)(2))+")/("+sci2exp(tf(j)(3))+")"])
                    
            catch
                message(["Scilab n a pas réussi à linéariser le système" "Vérifier que tous les blocs sont linéaires et causaux"])
                erreur1=%t
                continueSimulation = %f
                return
            end                

        end

        //------------Tracé-----------------------
        // On choisit le diagramme à tracer
        h=figure();
        set(h,"background",8)
        name_diag=["Bode","Black","Nyquist","placement de pôles - Evans"]
        h.figure_name="Diagrammes de "+name_diag(type_diag(i));
        drawlater();

        if num_w(i)>0 then
            omega=logspace(log10(w_min(i)/(2*%pi)),log10(w_max(i)/(2*%pi)),num_w(i));
        end
        
        h_list=list()
        legende_freq=[]
        for j=1:nb_inputs
            h1=tf(j)
            if exists('iodelaylib') then
               h1=iodelay(tf(j),total_delay_BO(j));
            end
            h_list($+1)=h1
            legende_freq($+1)='Entrée : '+inputs(j)+' Sortie : '+outputs(j)
        end
        
        //artifice nécessaire car sinon problème de list avec iodelay !
        vec_h="["
        for j=1:nb_inputs
            vec_h=vec_h+"h_list("+string(j)+")"
            if j~= nb_inputs then
                vec_h= vec_h+";"
            end
        end
        vec_h=vec_h+"]"
        //fin artifice
        //disp(vec_h)
        
        if type_diag(i)==1 then

            execstr("bode("+vec_h+",omega,legende_freq)")
            //bode(h_list,omega,legende_freq)
            bode_Hz2rad_2(h,marges(i));
                if marges(i)==1 then
                    if nb_inputs==1 & total_delay_BO(1)==0
                        freq_analysis(sys(j),'bode')
                    else
                        messagebox('L''affichage des marges n''est pour l''instant disponible que pour une seule courbe, sans retard')
                    end
                end
                if (asymp(i)==1) then
                    color_asympt=['k--','b--','g--','c--','r--']
                    for j=1:nb_inputs
                        rep=modulo(j,5)
                        if rep==0 then
                            rep=1
                        end
                        bode_asymp(sys(j),w_min(i),w_max(i),color_asympt(rep));
                    end
                    
                end  

        
        elseif type_diag(i)==2 then
            execstr("black("+vec_h+",omega,legende_freq)")
            //black(h_list,omega,legende_freq)

            black_Hz2rad(h)

            nicholschart(colors=color('light gray')*[1 1])

            if marges(i)==1 then
                if nb_inputs==1 & total_delay_BO(1)==0 then
                        freq_analysis(sys(j),'black')
                    else
                        messagebox('L''affichage des marges n''est pour l''instant disponible que pour une seule courbe, sans retard')
                    end
            end

elseif type_diag(i)==3 then
            if exists('iodelaylib') then
                disp("La symetrie n''est pas prise en compte avec iodelay chargée")
               execstr("nyquist("+vec_h+",w_min(i)/(2*%pi),w_max(i)/(2*%pi),legende_freq)")
            else
               execstr("nyquist("+vec_h+",w_min(i)/(2*%pi),w_max(i)/(2*%pi),legende_freq,sym(i))")
            end
            execstr("nyquist("+vec_h+",w_min(i)/(2*%pi),w_max(i)/(2*%pi),legende_freq)")
            //nyquist(h_list,w_min(i)/(2*%pi),w_max(i)/(2*%pi),legende_freq,sym(i))
            nyquist_Hz2rad(h)
            xtitle("")
            
             if marges(i)==1 then
                if nb_inputs==1 & total_delay_BO(1)==0 then
                        freq_analysis(sys(j),'nyquist')
                    else
                        messagebox('L''affichage des marges n''est pour l''instant disponible que pour une seule courbe, sans retard')
                    end
            end
            
//            ax=gca();
//            disp(ax.children)
//            ok=datatipInitStruct(ax,"formatfunction","formatNyquistTip");
//            
//            if use_iodelay then
//                nyquist(h_list,w_min(i)/(2*%pi),w_max(i)/(2*%pi))
//                //legend(h.children,legende_freq)
//            else
//                nyquist(h_list,w_min(i)/(2*%pi),w_max(i)/(2*%pi),1/num_w(i),legende_freq)    
//            end
        elseif type_diag(i)==4 then
            drawnow()
            close()
            for j=1:nb_inputs
                h=figure();
                set(h,"background",8)
                h.figure_name="Lieu  d''Evans de la fonction de transfert entre " + inputs(j) + " et " + outputs(j);
                evans(clean(tf(j)),0.0001)
                h.children(1).children(1).legend_location='lower_caption'
                sgrid()
            end
        end

        drawnow();

    end

    // Force Simulation to start after pre_simulation process.
    continueSimulation = %t
endfunction




function [num,den]=pade(ex,p,q)
    // Calcul une fct de transfert approchée de l'exponentielle : e^ex
    // à l'ordre p au numérateur et q au dénominateur

    // ex : un polynome
    // p et q des entiers

    //Coefficients du numérateur
    num=0
    den=0
    for i=0:p
        coef=prod(1:p)*prod(1:(p+q-i))/(   prod(1:(p-i))*prod(1:(p+q))*prod(1:i) );
        //mprintf("h : coeff x^%i : %f\n",i,coef)
        num=num+coef*ex^i;   // pour tenir compte de tau : coef*(-tau*s)^i
    end

    //Coefficients du dénominateur
    for i=0:q
        coef=(-1)^i*prod(1:q)*prod(1:(p+q-i))/(   prod(1:(q-i))*prod(1:(p+q))*prod(1:i) );
        //mprintf("k : coeff x^%i : %f\n",i,coef)
        den=den+coef*ex^i;   // pour tenir compte de tau : coef*(-tau*s)^i
    end
endfunction


function []=bode_asymp (ss,w_min,w_max,color_asympt)
    h=ss2tf(ss)

    racine_num = roots(h.num);
    racine_den = roots(h.den);

    if (find(racine_num==0)) 
        disp('Problem class of system is negative') //Scilab veut pas ce cas de toute facon
    end
    rac_nul = find(racine_den==0)
    alpha = length(rac_nul)
    s=poly(0,'s');
    K = horner(h*s^alpha   ,0)

    racine_den(rac_nul)=[]; //suppression des 0

    if (length([find(abs(racine_num)>1e8) find(abs(racine_num)<1e-8)])>0) then
        disp('Extreme root removed : '+string(racine_num([find(abs(racine_num)>1e8) find(abs(racine_num)<1e-8)])'))
        racine_num([find(abs(racine_num)>1e8) find(abs(racine_num)<1e-8)])=[]
    end
    if (length([find(abs(racine_den)>1e8) find(abs(racine_den)<1e-8)])>0) then
        disp('Extreme root removed : '+string(racine_den([find(abs(racine_den)>1e8) find(abs(racine_den)<1e-8)])'))
        racine_den([find(abs(racine_den)>1e8) find(abs(racine_den)<1e-8)])=[]
    end

    //    racine = [racine_num; racine_den];
    //    disp(racine)

    i=1;
    puls = [];
    order = [];
    while i <= length(racine_num) //real root
        if (isreal(racine_num(i),0)) then
            puls = [puls -racine_num(i)];
            order = [order 1];
            i=i+1;
        else //complex root
            xi=1/sqrt(1+(imag(racine_num(i))/real(racine_num(i)))^2);
            puls = [puls -real(racine_num(i))/xi];
            i=i+2;
            order = [order 2];
        end
    end
    i=1;
    while i <= length(racine_den) //real root
        if (isreal(racine_den(i),0)) then
            puls = [puls -racine_den(i)];
            order = [order -1];
            i=i+1;
        else //complex root
            xi=1/sqrt(1+(imag(racine_den(i))/real(racine_den(i)))^2);
            puls = [puls -real(racine_den(i))/xi];
            order = [order -2];
            i=i+2;
        end
    end

    [puls,ind]=gsort(puls,'g','i')
    order = order(ind)



    asym = [-20*alpha];
    phas = [-90*alpha];
    i=1;
    while(i<=length(puls))
        new_dir = asym($)+order(i)*20;
        asym = [asym new_dir]
        new_arg = phas($)+order(i)*90;
        phas = [phas new_arg];
        i=i+1
    end

    //bode(h)
    fig=gcf()
    sca(fig.children(2))

    //    if(length(puls)==0) then
    //        wmin=w_min;
    //        wmax=w_max;
    //    else
    //        wmin=min(real(puls(1)/10),w_min) ;
    //        wmax=max(real(puls($)*10),w_max);
    //    end
    //
    //  puls=[wmin puls wmax]; 

    //change DV : asymptotes seulement entre les bornes
    wmin=w_min
    wmax=w_max
    puls_to_plot=[]

    for p=real(puls)
        if p>=wmin & p<=wmax then
            puls_to_plot($+1)=p
        end
    end
    puls=[wmin puls_to_plot' wmax];
    // end change DV        

    eq_asymp = [20*log10(K/wmin^alpha)];
    puls_p=[];
    phas($+1)=phas($);
    eq_phas =[phas(1)];
    i=2
    while (i<=length(puls))
        eq_asymp = [eq_asymp eq_asymp($)+asym(i-1)*log10(puls(i)/puls(i-1))]

        puls_p =[puls_p puls(i-1) puls(i)]
        eq_phas =[eq_phas phas(i-1) phas(i)]
        i=i+1
    end

    plot(puls,eq_asymp,color_asympt)
    sca(fig.children(1))
    plot(puls_p,eq_phas(1:$-1),color_asympt)

endfunction








