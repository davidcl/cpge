// This GUI file is generated by guibuilder version 2.2
//////////
f=figure('figure_position',[400,50],'figure_size',[319,140],'auto_resize','on','background',[33],'figure_name','Figure n°%d');
//////////
delmenu(f.figure_id,gettext('File'))
delmenu(f.figure_id,gettext('?'))
delmenu(f.figure_id,gettext('Tools'))
toolbar(f.figure_id,'off')
sleep(100)
handles.dummy = 0;
handles.slider_KP=uicontrol(f,'unit','normalized','BackgroundColor',[0.8,0.8,0.8],'Enable','on','FontAngle','normal','FontName','helvetica','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[0,0,0],'HorizontalAlignment','center','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.05375,0.3943249,0.5203125,0.4450098],'Relief','flat','SliderStep',[0.01,0.1],'String','KP','Style','slider','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','slider_KP','Callback','slider_KP_callback(handles)')
handles.text_KP=uicontrol(f,'unit','normalized','BackgroundColor',[0.8,0.8,0.8],'Enable','on','FontAngle','normal','FontName','helvetica','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[0,0,0],'HorizontalAlignment','center','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.6223288,0.4264865,0.3,0.4372072],'Relief','flat','SliderStep',[0.01,0.1],'String','value = 0%','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','text_KP','Callback','')


//////////
// Callbacks are defined as below. Please do not delete the comments as it will be used in coming version
//////////

function slider_KP_callback(handles)
//Write your callback for  slider_KP  here
    handles.text_KP.String="value = "+string(round(100*handles.slider_KP.Value))+"%"
endfunction


