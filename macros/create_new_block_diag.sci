//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010-2010 - DIGITEO -
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [scs,presence_delay,param_delays,err]=create_new_block_diag(scs_m,input_signal,output_signal)

    err=0; //gestion des erreurs
    scs=scs_m; // On repart sur un diagramme propre
    num_in=[] // le numéro du bloc d'entrée dans le diagramme original
    num_out=[] // Le numéro du bloc de sortie dans le diagramme original
    //Parametres pour la gestion des blocs retard
    param_delays.delay_parameters=[]; //valeur des TIME_DELAY 
    param_delays.num_delay=[]; //stockage des numéros de blocs TIME_DELAY

    param_delays.BO_delay=[]; //flag indiquant si le bloc est dans la BO ou dans une BF   

    objs = objs;

    //---------- Traitement des blocs retard TIME_DELAYS-----------
    // On cherche et on remplace les blocs TIME_DELAYS

    for j = 1:size(objs)
        curObj= objs(j);
        if (typeof(curObj)=='Block'  & curObj.gui=="TIME_DELAY") then
            presence_delay=%t;
            param_delays.num_delay($+1)=j;
            param_delays.delay_parameters($+1)=curObj.model.rpar(1);
            param_delays.BO_delay($+1)=1;
            //effacement du bloc TIME_DELAY et remplacement par un fct de transfert égale à un gain pur au début
            //                gain=GAINBLK_f('define')
            //                gain.graphics.pin = curObj.graphics.pin;
            //                gain.graphics.pout = curObj.graphics.pout;
            //                objs(j)=gain;
            approx=CLR('define')
            approx.graphics.pin = curObj.graphics.pin;
            approx.graphics.pout = curObj.graphics.pout;
            approx.graphics.exprs=["1";"1"];
            approx.model.rpar=1;
            objs(j)=approx;
        end
    end

    //---------- Traitement de l'entrée -----------
    // On récupère le bloc grandeur physique d'entrée

    for j = 1:size(objs)
        curObj= objs(j);
        if (typeof(curObj) == "Block" & curObj.gui == "GRANDEUR_PHYSIQUE")
            if curObj.graphics.exprs==input_signal then
                num_in=j;
            end
        end
    end
    
    if num_in==[] then
        messagebox('La grandeur physique '+input_signal+' n ''est pas définie')
        err=1
        return
    end
    //On recalcule le nombre de blocs
    nombre_blocs=0
    for iii=1:size(objs)
        if typeof(objs(iii))=='Block' then
            nombre_blocs=nombre_blocs+1;
        end
    end
    nombre_liens=size(objs)-nombre_blocs;  //calcul du nombre de liens


    // On décale tous les liens d'un cran
    for k=1:nombre_liens
        objs(nombre_blocs+nombre_liens+2-k)=objs(nombre_blocs+nombre_liens+1-k);
    end

    //On décale tout les pin et pout des blocs pour rester en concordances avec les liens
    for l=1:nombre_blocs
        if ~isempty(objs(l).graphics.pin) then objs(l).graphics.pin=objs(l).graphics.pin+1
        end
        if ~isempty(objs(l).graphics.pout) then objs(l).graphics.pout=objs(l).graphics.pout+1
        end
        if ~isempty(objs(l).graphics.pein) then objs(l).graphics.pein=objs(l).graphics.pein+1
        end
        if ~isempty(objs(l).graphics.peout) then objs(l).graphics.peout=objs(l).graphics.peout+1
        end
    end

    // On crée un bloc grandeur physique d'entrée en tant qu'objet nombre blocs+1 avec le numéro d'entrée correspondant
    nombre_blocs=nombre_blocs+1;   // Le nombre de bloc est incrémenté
    objs(nombre_blocs)=IN_f('define') // Crée un nouveau bloc IN_f
    //objs(nombre_blocs).graphics.exprs=scs_m.objs(num_in).graphics.exprs  // On récupère le numéro de l'entrée
    // On remplace le blocs IN_Bode par un GOTO (ce qui vient avant le bloc grandeur physique est alors inutilisé)
    objs(num_in)=GOTO('define');
    // On relie les nouveaux blocs aux liens existants et au nouveau lien
    // pour le bloc GOTO (remplace bloc grandeur physique d'entrée)
    objs(num_in).graphics.orig=scs_m.objs(num_in).graphics.orig;
    old_num_pin= scs_m.objs(num_in).graphics.pin(1);
    objs(num_in).graphics.pin= old_num_pin+1; //on garde le numero du lien d'entrée de l'ancien bloc + 1 (car ajout d'un bloc)
    objs(num_in).graphics.sz= [40,40];
    // pour le bloc IN_f
    objs(nombre_blocs).graphics.orig=[scs_m.objs(num_in).graphics.orig(1)+30,scs_m.objs(num_in).graphics.orig(2)-30];
    objs(nombre_blocs).graphics.sz=[40,40];
    old_num_pout=scs_m.objs(num_in).graphics.pout(1);
    objs(nombre_blocs).graphics.pout=old_num_pout+1;
    // On modifie le from-to du lien correspondant
    objs(old_num_pout+1).from=[nombre_blocs 1 0];
    //---------- Traitement de l'entrée -----------

    //---------- Traitement de la sortie ----------
    // On récupère le bloc grandeur physique de sortie
    for j = 1:size(objs)
        curObj= objs(j);
        if (typeof(curObj) == "Block" & curObj.gui == "GRANDEUR_PHYSIQUE")
            if curObj.graphics.exprs==output_signal then
                num_out=j;
            end
        end
    end
    if num_out==[] then
        messagebox('La grandeur physique '+output_signal+'n ''est pas définie')
        err=1
        return
    end
    // On décale tout les liens d'un cran
    for k=1:nombre_liens
        objs(nombre_blocs+nombre_liens+2-k)=objs(nombre_blocs+nombre_liens+1-k);
    end

    //On décale tout les pin et pout des blocs pour rester en concordances avec les liens
    for l=1:nombre_blocs
        if ~isempty(objs(l).graphics.pin) then objs(l).graphics.pin=objs(l).graphics.pin+1
        end
        if ~isempty(objs(l).graphics.pout) then objs(l).graphics.pout=objs(l).graphics.pout+1
        end
        if ~isempty(objs(l).graphics.pein) then objs(l).graphics.pein=objs(l).graphics.pein+1
        end
        if ~isempty(objs(l).graphics.peout) then objs(l).graphics.peout=objs(l).graphics.peout+1
        end
    end

    // On crée un bloc OUT_f en tant qu'objet nombre blocs+1 avec le numéro de sortie correspondant
    nombre_blocs=nombre_blocs+1;   // Le nombre de bloc est incrémenté
    objs(nombre_blocs)=OUT_f('define') // Crée un nouveau bloc out_f
    //objs(nombre_blocs).graphics.exprs=scs_m.objs(num_out).graphics.exprs  // On récupère le numéro de l'entrée

    // On remplace le blocs grandeur physique de sortie par un bloc CONST_m 
    objs(num_out)=CONST_m('define');//

    // On relie les nouveaux blocs aux liens existants
    //Pour le bloc CONST_m
    objs(num_out).graphics.orig=scs_m.objs(num_out).graphics.orig
    old_num_pout=scs_m.objs(num_out).graphics.pout(1);
    objs(num_out).graphics.pout= old_num_pout+2;

    //Pour le bloc OUT_f
    objs(nombre_blocs).graphics.orig=[scs_m.objs(num_out).graphics.orig(1)+10,scs_m.objs(num_out).graphics.orig(2)-30]
    objs(nombre_blocs).graphics.sz=[40,40]
    old_num_pin=scs_m.objs(num_out).graphics.pin(1);
    objs(nombre_blocs).graphics.pin= old_num_pin+2;
    //On modifie le from-to du lien correspondant
    objs(old_num_pin+2).to=[nombre_blocs 1 1]
    //---------- Traitement de la sortie ----------


    scs.objs = objs;
endfunction

