//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function block=NI600X_C_READ_sim(block,flag)
    
    global taskAI0
  function DEBUG(message)
      disp("[DEBUG time = "+string(scicos_time())+"] {"+block.label+"} NI600X_C_READ Simulation: "+message);
  endfunction
  
      
  


  select flag
     case -5 // Error

     case 0 // Derivative State Update


     case 1 // Output Update
     periode=10
     //disp(periode)
             [data,pointsRead,err] = DAQ_ReadAnalogF64(taskAI0,-1,periode,...
                        DAQ("Val_GroupByChannel"),1);
                        DAQ_ErrChk(taskAI0, err); 
//disp(data)
//disp(err)
//sleep(periode/1000)

      block.outptr(1) = data;
      //disp(scicos_time())


     case 2 // State Update

     case 3 // OutputEventTiming
      //DEBUG("OutputEventTiming")
      //pause
      //evout = block.evout(1);
      //if evout < 0
      //    evout = 0.001;
      //else
      //    evout = evout + 0.001;
      //end
      //block.evout(1) = evout;

    case 4 // Initialization


     //créer la tache 
//    
//     nodevice=block.rpar(1)
//     device  = "Dev"+string(nodevice);
//     chan=block.rpar(3)
//     channel = "Dev"+string(nodevice)+"/ai"+string(chan);
//      // Valeurs limites des voies (en V)
//        maxVal = 10.0;
//        minVal = -10.0;
//        rate=0.01;
//
//source = "OnboardClock";
//// Paramètres de lecture des données
//// Taille du stockage (en nombre d'échantillons)
//// Pour une acquisition continue, il faut prévoir un buffer suffisamment
//// large de manière à ce qu'il puisse amortir un ralentissement du 
//// traitement.
//
//
//// Nombre de points à lire = taille du buffer ou nombre d'échantillons par voie
//// En conséquence (à vérifier), peut-être prendre pointsToRead = bufferSize;
//
// 
//// Durée (en s) pour écrire tous les échantillons : valeur par défaut à 10 s
//// Si timeout = -1, attente infinie
//// Si timeout = 0, un essai de lecture puis renvoi d'erreur si pas de retour
//// On prend une valeur supérieure à la période d'acquisition juste pour être 
//// tranquille sans rien planter
//// Durée de la mesure, valeur définie en s
////duree = 5; 
//timeout = 0;
//[err] = DAQ_ResetDevice(device);
//[taskAI,err] = DAQ_CreateTask(""); DAQ_ErrChk(taskAI,err);
////[err] = DAQ_CreateAIVoltageChan(taskAI, chan, DAQ("Val_RSE"),...
////        minVal, maxVal);
//[err] = DAQ_CreateAIVoltageChan(taskAI, channel, DAQ("Val_Diff"),...
//        minVal, maxVal);        
//      
//        DAQ_ErrChk(taskAI,err);
//// Val_FiniteSamps pour N echantillons et Val_ContSamps pour mode continu
//[err] = DAQ_CfgSampClkTiming(taskAI, source, rate,...
//        DAQ("Val_Rising"), DAQ("Val_FiniteSamps"),2);
//        DAQ_ErrChk(taskAI,err);
//     
//[err] = DAQ_StartTask(taskAI); DAQ_ErrChk(taskAI,err);


     case 5 // Ending
DAQ_StopTask(taskAI0);
DAQ_ClearTask(taskAI0);

     case 6 // Re-Initialisation

     case 9 // ZeroCrossing

    else // Unknown flag

    end
endfunction