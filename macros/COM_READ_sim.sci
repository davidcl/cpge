//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function block=COM_READ_sim(block,flag)
  function DEBUG(message)
      disp("[DEBUG time = "+string(scicos_time())+"] {"+block.label+"} COM_READ Simulation: "+message);
  endfunction
  select flag
     case -5 // Error

     case 0 // Derivative State Update


     case 1 // Output Update
      DEBUG("Output update");
      // FIXME: Mettre ici tout ce qui sert a lire sur le port serie

      //y1 = PIC_receive();
      //block.outptr(1) = y1;

      // Pour tester
      block.outptr(1) = [300 * rand() ; 1000 * rand() ; 13 * rand() ; scicos_time()];

     case 2 // State Update

     case 3 // OutputEventTiming
      DEBUG("OutputEventTiming")
      //pause
      evout = block.evout(1);
      if evout < 0
          evout = 0.001;
      else
          evout = evout + 0.001;
      end
      block.evout(1) = evout;

     case 4 // Initialization

     case 5 // Ending
     // FIXME: quoi faire a la fin de la simulation

     // PIC_put_zero();

     case 6 // Re-Initialisation

     case 9 // ZeroCrossing

    else // Unknown flag

    end
endfunction