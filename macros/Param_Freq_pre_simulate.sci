//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010-2010 - DIGITEO -
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//



function []=Param_Freq_pre_simulate(scs_m, needcompile)
    scs=[]
    // On recopie le scs_m
    scs=scs_m;
    // Retrieve all objects
    objs = scs_m.objs;

    nombre_blocs=0;    //Nombre de blocs dans le diagramme
    nombre_liens=0;    //Nombre de lien dans le diagramme
    nombre_rep_freq=0; //Nombre de blocs REP_FREQ dans le diagramme
    nombre_grandeur_phy=0;  //Nombre de blocs IN_Bode dans le diagramme
    num_grandeur_phy=[];    //Numéro des blocs grandeur physique dans le diagramme
    input_signals=[];   //vecteur des grandeurs d'entrée
    output_signals=[];   // vecteur des grandeurs de sortie
    type_diag=[];        // vecteur des types de diagrammes à tracer
    //    nombre_out_bode=0;  //Nombre de blocs OUT_Bode dans le diagramme
    //    num_out_bode=[];    //Numéro des blocs OUT_bode dans le diagramme
    wmin=0;
    wmax=0;
    num_w=0;            // Nombre de fréquences à tracer
    omega=[]             //vecteur des pulsations à tracer dans la réponse fréquentielle

    //Récupère le nombre de blocs dans le modèle
    for i=1:size(objs)
        if typeof(objs(i))=='Block' then
            nombre_blocs=nombre_blocs+1;
        end
    end
    nombre_liens=size(objs)-nombre_blocs;  //calcul du nombre de liens

    // Récupère les blocs REP_freq et les paramètres associés
    for i=1:nombre_blocs
        if typeof(objs(i)) == "Block" && objs(i).gui=="REP_FREQ" then
            nombre_rep_freq=nombre_rep_freq+1;
            num_rep_freq(nombre_rep_freq)=i;
            input_signals(nombre_rep_freq)=objs(i).model.opar(1);
            output_signals(nombre_rep_freq)=objs(i).model.opar(2);
            type_diag(nombre_rep_freq)=objs(i).model.ipar;
            w_min=objs(i).model.rpar(1);
            w_max=objs(i).model.rpar(2);
            if exists("objs(i).model.rpar(3)") then
                if ~isempty(objs(i).model.rpar(3)) then
                    num_w=objs(i).model.rpar(3);
                end
            end
        end
    end

    // s'il y a déja des blocs IN_f et OUT_f, on demande à l'utilisateur de les retirer
    probleme_in_f=%f
    for i=1:nombre_blocs
        if typeof(objs(i)) == "Block" && or(objs(i).gui ==  ["IN_f" "OUT_f"]) then
            probleme_in_f=%t;
        end
    end
    if probleme_in_f then disp('Le tracé des réponses fréquentielles est incompatible avec la présence de blocs IN_f et OUT_f')
    end


    //-------------------------- Traitement à proprement parler
    // Pour chacun des bloc REP_FREQ trouvés dans le diagramme

    for i=1:nombre_rep_freq   //i est le bloc rep_freq que l'on traite
        presence_delay=%f
            delay_parameters=[]; //valeur des TIME_DELAY 
            num_delay=[]; //stockage des numéros de blocs TIME_DELAY
            BO_delay=[]; //flag indiquant si le bloc est dans la BO ou dans une BF        
        
        inputs=stripblanks(strsplit(input_signals(i),';'))
        outputs=stripblanks(strsplit(output_signals(i),';'))
        nb_inputs=size(inputs,1)
        if size(inputs,1)>1 then
            messagebox('La superposition de diagrammes fréquentiels en paramétrique n''est pas encore accessible. N''utilisez qu''un seul couple de grandeurs physiques')
            erreur1=%t;
            continueSimulation = %t
            return
        end
        if type_diag(i)==4 then
            messagebox('Le diagramme d''Evans n''est pas disponible en paramétrique')
            erreur1=%t;
            return
        end
        //modification du schema-bloc
        [scs,presence_delay,param_delays,err]=create_new_block_diag(scs_m,input_signals(i),output_signals(i))
        if err==1 then
            return
        end

        if presence_delay==%t then
            messagebox('L''utilisation de blocs retard n''est pas encore implanté dans le cadre d''une étude paramétrique. Ces blocs sont remplacés par des gains unitaires.')
        end
            num_delay=param_delays.num_delay;
            delay_parameters=param_delays.delay_parameters;
            BO_delay=param_delays.BO_delay;

        // On récupere le numéro du bloc PARAM_VAR puis les caractéristiques correspondantes
        for ii = 1:size(scs.objs)
            curObj= scs.objs(ii);
            if (typeof(curObj) == "Block" & curObj.gui == "PARAM_VAR")
                if length(curObj.model.opar)>0 then
                    param1=curObj.model.opar(1)
                    param1val=curObj.model.opar(2)
                end
                if length(curObj.model.opar)>2 then
                    param2=curObj.model.opar(3)
                    param2val=curObj.model.opar(4)
                end
                if length(curObj.model.opar)>4 then
                    param3=curObj.model.opar(5)
                    param3val=curObj.model.opar(6)
                end
            end
        end

        num_param=0;
        if ~isempty(param1) then num_param=num_param+1;
        end
        if ~isempty(param2) then num_param=num_param+1;
        end
        if ~isempty(param3) then num_param=num_param+1;
        end

        // Pour chacune des valeur des parametre il faut linéariser et calculer la fonction de transfert
        Nsimu=0  // Numero de la simu
        x=0      // pourcentage d'avance des calculs
        winId=waitbar('Simulation in progress')

        select num_param
        case 1 //1 parametre

            for ii=1:length(param1val)
                x=(ii)/length(param1val)
                waitbar(x,winId);
                //evaluation du contexte
                context=scs.props.context;
                // Trouve la variable a remplacer dans le contexte
                //pause
                cIndex = grep(context, "/"+string(param1)+"[ ]*=/", "r");
                
                context(cIndex)=string(param1)+"="+string(param1val(ii))
                [%scicos_context, ierr] = script2var(context,struct())
                %cpr=list();
                scs.props.context = context;

                [scs2, %cpr, needcompile, ok] = do_eval(scs, %cpr,%scicos_context);
                
                
                
                Nsimu=Nsimu+1;
                commande='sys'+string(Nsimu)+'=lincos(scs2)'
                try
                    execstr(commande)
                catch
                    message('Scilab n''a pas réussi à déterminer la fonction de transfert du système. Vérifiez que tous les blocs sont linéaires et causaux')
                end
                legend_by_simul(Nsimu)=string(param1)+'='+string(param1val(ii));
            end

        case 2
            Nsimu=0;
            context=scs.props.context;
            for ii=1:length(param1val)
                context($+1)=string(param1)+"="+string(param1val(ii))
                for jj=1:length(param2val)
                    x=((ii-1)*length(param2val)+(jj-1))/(length(param1val)*length(param2val));
                    waitbar(x,winId);
                    context($+1)=string(param2)+"="+string(param2val(jj))
                    [%scicos_context, ierr] = script2var(context,struct())
                    %cpr=list();
                    [scs2, %cpr, needcompile, ok] = do_eval(scs, %cpr,%scicos_context);
                    Nsimu=Nsimu+1;
                    commande='sys'+string(Nsimu)+'=lincos(scs2)'
                    try
                        execstr(commande)
                    catch
                        message('Scilab n a pas réussi à déterminer la fonction de transfert du système. Vérifiez que tous les blocs sont linéaires et causaux')
                    end
                    //stockage du nom de la simulation avec la valeur du parametre
                    legend_by_simul(Nsimu)=string(param1)+'='+string(param1val(ii))+', '+string(param2)+'='+string(param2val(jj));

                end
            end

        case 3
            Nsimu=0;
            context=scs.props.context;

            for ii=1:length(param1val)
                context($+1)=string(param1)+"="+string(param1val(ii))
                for jj=1:length(param2val)
                    context($+1)=string(param2)+"="+string(param2val(jj))
                    for kk=1:length(param3val)
                        x=((ii-1)*length(param2val)*length(param3val)+(jj-1)*length(param3val)+(kk-1))/(length(param1val)*length(param2val)*length(param3val));
                        waitbar(x,winId);
                        context($+1)=string(param3)+"="+string(param3val(kk))
                        [%scicos_context, ierr] = script2var(context,struct())
                        %cpr=list();
                        [scs2, %cpr, needcompile, ok] = do_eval(scs, %cpr,%scicos_context);
                        Nsimu=Nsimu+1;
                        commande='sys'+string(Nsimu)+'=lincos(scs2)'
                        try
                            execstr(commande)
                        catch
                            message('Scilab n a pas réussi à déterminer la fonction de transfert du système. Vérifiez que tous les blocs sont linéaires et causaux')
                        end
                        //stockage du nom de la simulation avec la valeur du parametre
                        legend_by_simul(Nsimu)=string(param1)+'='+string(param1val(ii))+', '+string(param2)+'='+string(param2val(jj))+', '+string(param3)+'='+string(param3val(kk));
                    end
                end
            end

        end
        delete(winId);

        //---------- Trace en fonction du type de diagramme choisi -------------------
        // On construit le vecteur des omega
        if num_w>0 then
            omega=logspace(log10(w_min/(2*%pi)),log10(w_max/(2*%pi)),num_w);
        end

        // On choisit le diagramme à tracer
        h=figure();
        set(h,"background",8)

        drawlater();
        nom_type_diag=["bode","black","nyquist"]
        h.figure_name="Diagrammes de "+nom_type_diag(type_diag(i))+" : Entrée "+input_signals(i)+" Sortie "+output_signals(i);
        commande_diag=nom_type_diag(type_diag(i))+"([";
        for kk=1:Nsimu
            if kk~= Nsimu then
                commande_diag=commande_diag+'sys'+string(kk)+';'
            else
                commande_diag=commande_diag+'sys'+string(kk)
            end
        end
        //adaptation de la commande de tracé pour Nyquist
        if type_diag(i)==3 then
            commande_diag=commande_diag+'],w_min/(2*%pi),w_max/(2*%pi),legend_by_simul,%f)'
        else
            commande_diag=commande_diag+'],w_min/(2*%pi),w_max/(2*%pi),legend_by_simul)'
        end

        execstr(commande_diag) //tracé
        if type_diag(i)==2 then
            if(h.children.data_bounds(1)>-180)
                h.children.data_bounds(1)=-180;
            end
            nicholschart(colors=color('light gray')*[1 1])
            black_Hz2rad(h)
        elseif type_diag(i)==1 then
            bode_Hz2rad_2(h,0)
        elseif type_diag(i)==3 then
            nyquist_Hz2rad(h)
        end
        drawnow();
    end

endfunction






