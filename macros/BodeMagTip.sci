//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2014 - David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function str=BodeMagTip(datatipHandle)
//this function is called by the datatips mechanism to format the tip
//string for the magnitude bode curves
    pt = datatipHandle.data(1:2);
    str = msprintf("%.4g"+_("rad/s")+"\n%.4g"+_("dB"), pt(1), pt(2));
endfunction
