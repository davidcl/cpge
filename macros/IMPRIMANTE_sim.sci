//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function block=IMPRIMANTE_sim(block,flag)
    global port_TCL; //port connection imprimante
  function DEBUG(message)
      disp("[DEBUG time = "+string(scicos_time())+"] {"+block.label+"} IMPRIMANTE Simulation: "+message);
  endfunction
  disp("val "+string(flag))
   select flag
     case -5 // Error

     case 0 // Derivative State Update


     case 1 // Output Update
      //DEBUG("Output update");
//      if connected==0 then
//          return
//      end
//      y1 = PIC_send(block.inptr(1)); //send data to serial
//      y1 = PIC_receive(); //receive data from serial
//      block.outptr(1) = y1;
      
      //test pour debug
      //disp(connected)
      
//      u1=block.inptr(1)
//      if ( abs(u1)>1023 ) then u1=sign(u1)*1023;
//      end
//      disp(u1)
//      //envoi de P + nombre sur 2 octets          
//      values= "P" + ascii((int(evstr(u1)/256))) + ascii(modulo(evstr(u1),256))
//      writeserial(port_TCL,values);
//      
//      //reception des données
//      [q,flags]=serialstatus(port_TCL)
//       while(q<8)
//         [q,flags]=serialstatus(port_TCL)
//       end
//       values=readserial(port_TCL);
//       data=ascii(values)';
//
//       y1(1)=data(1)*256+data(2); //position
//       y1(2)=data(3)*256+data(4); //delta position
//       y1(3)=data(5)*256+data(6);//deltaT
//       y1(4)=data(7)*256+data(8);//tension
//       y1=double(int16(y1));
//       vit=y1(2)/y1(3); //vitesse
//       disp(y1)
//      block.outptr(1) = [y1(1),vit];
block.outptr(1) = [0,0];
      disp(scicos_time())
     case 2 // State Update


     case 3 // OutputEventTiming

     case 4 // Initialization

     case 5 // Ending
 disp("end")
       values= "P" + ascii(0) + ascii(0)
       writeserial(port_TCL,values);
closeserial(port_TCL);
     case 6 // Re-Initialisation

     case 9 // ZeroCrossing

    else // Unknown flag

    end


endfunction
