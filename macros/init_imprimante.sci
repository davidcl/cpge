
function continueSimulation=init_imprimante(block_imprimante)
    global port_TCL;
    
    port_com_imprimante=block_imprimante.model.rpar(2);
    connected=0;
    disp(port_com_imprimante)
    try
        closeserial(port_TCL);
        sleep(100);
        disp("port disconnected")
    end
    try
        port_TCL=openserial(port_com_imprimante,"115200,n,8,1","binary","none");
        connected=1;
        sleep(1000);
        disp("connected")
    catch
        messagebox("Impossible de se connecter à l''imprimante. Choisir un port différent")
        continueSimulation=%f
        return
    end
    if (connected == 1) then
        //Envoie d'une demande de démarrage de travail
        values=readserial(port_TCL);
        nbpoint = 1000;
        writeserial(port_TCL,"T"+ascii((int(evstr(nbpoint)/256))) + ascii(modulo(evstr(nbpoint),256)));
        //Attente de la reception de la trame de DEBUT
        values="";
        while (length(strindex(values,"BEG;")) == 0 )
            sleep(10);
            values=values+readserial(port_TCL);
            //disp(values)
        end
        disp('Initialisation terminée')
    end

endfunction


