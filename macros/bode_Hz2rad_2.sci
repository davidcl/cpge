function []=bode_Hz2rad_2(h,marges)
//Cette fonction permet de modifier les diagramme de Bode pour un affichage en rad/s et non plus en Hz
//h est un hanlde d'une figure contenant des diagrammes de Bode
//
//

//k=1 phase, k=2 gain
labels=["Phase (°)";"Amplitude (dB)"]
tips=["BodePhaseTip";"BodeMagTip"]
pos_h=[9,5];
for k=1:2
    for i=1:size(h.children(k).children,1)
        if(h.children(k).children(i).type=="Compound")
            for j=1:size(h.children(k).children(i).children,1)
                h.children(k).children(i).children(j).display_function=tips(k)
                h.children(k).children(i).children(j).data(:,1)=h.children(k).children(i).children(j).data(:,1)*2*%pi;
            end
            
            //h.children(k).title.text=h.children(k).y_label.text;
            xmin1=h.children(k).data_bounds(1)*2*%pi;
            xmax1=h.children(k).data_bounds(2)*2*%pi;
            ymin1=h.children(k).data_bounds(3);
            ymax1=h.children(k).data_bounds(4);
            if (marges==1 & k==1) then
                ymin1=min(ymin1,-180);
            end
            
            rect=[xmin1,ymin1,xmax1,ymax1]
            nb_dec=log(xmax1/xmin1)/log(10);
            h.children(k).x_label.text="Pulsation (rad/s)"
            h.children(k).x_location="origin";
            coef=3;
            if k==1 then
                height=(ymax1-ymin1)/10;
                if ymin1<0 & ymax1>=0  then
                    h.children(k).x_label.position=[-coef*log((nb_dec-1)/(nb_dec))*xmax1;0-height];
                elseif ymin1<0 & ymax1<0  then
                    h.children(k).x_label.position=[-coef*log((nb_dec-1)/(nb_dec))*xmax1;ymax1-height];
                elseif ymin1>=0  then
                    h.children(k).x_label.position=[-coef*log((nb_dec-1)/(nb_dec))*xmax1;ymin1+height];                    
                end
            else
                height=(ymax1-ymin1)/30;
                if ymin1>0 then
                    h.children(k).x_label.position=[-coef*log((nb_dec-1)/(nb_dec))*xmax1;ymin1+height] 
                else
                    if ymax1>0 & ymax1>abs(ymin1) then
                         h.children(k).x_label.position=[-coef*log((nb_dec-1)/(nb_dec))*xmax1;0+height]
                     else
                        h.children(k).x_label.position=[-coef*log((nb_dec-1)/(nb_dec))*xmax1;0-height]                  
                    end

                end
            end
            h.children(k).y_label.text=labels(k);
            replot(rect,h.children(k))
            
        end
    end
end

endfunction

