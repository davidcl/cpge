//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010-2010 - DIGITEO -
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function scs_m=REP_TEMP_pre_simulate(scs_m)

	nombre_blocs = 0;
	nombre_REP_TEMP = 0;
	//extraction du temps de simulation et du nombre de points pour le calcul
	for i=1 : size(scs_m.objs)
		if typeof(scs_m.objs(i)) == "Block" then
			nombre_blocs = nombre_blocs + 1;
			if scs_m.objs(i).gui == "REP_TEMP" then
				nombre_REP_TEMP = nombre_REP_TEMP + 1;
				num_pts = scs_m.objs(i).model.rpar(1);
				tf = scs_m.objs(i).model.rpar(2);
			end;
		end;
	end;

	if nombre_REP_TEMP>1 then
		disp("Avertissement : Une confusion peut être possible du fait qu''il y ait plusieurs block REP_TEMP. Nous vous conseillons de mettre qu''un seul block REP_TEMP.");
        return
	elseif nombre_REP_TEMP==0 then
        tf=1
        num_pts=200
    end;


	nb_scope = 0;
	nb_outputs = [];
	nb_total_outputs = 0;
	for i=1 : nombre_blocs
		obj = scs_m.objs(i);
		if typeof(obj) == "Block" then
			if obj.gui == "SCOPE" then
				nb_scope = nb_scope + 1;
				nb_outputs(nb_scope) = evstr(scs_m.objs(i).graphics.exprs(1));
				//recherche parmi les blocs la clock et le tows
				list_obj = scs_m.objs(i).model.rpar.objs;
				no = 1;
				for j = 1:size(list_obj)
					if (typeof(list_obj(j)) == "Block" & list_obj(j).gui == "TOWS_c") then //on affecte un nom pour le stockage dans scilab
						varname = "o"+string(no+nb_total_outputs);
						list_obj(j).graphics.exprs = [string(num_pts) ; varname ; "0"];
						//list_obj(j).model.ipar = [num_pts ; 2 ; ascii(varname)'];
						no=no+1;
					elseif (typeof(list_obj(j)) == "Block" & list_obj(j).gui == "SampleCLK") then //on modifie le pas de temps
						list_obj(j).graphics.exprs = [string(tf/num_pts);"0"];
						//list_obj(j).model.rpar = [tf/num_pts;0];  
					end;
				end;
				scs_m.objs(i).model.rpar.objs = list_obj;
				nb_total_outputs = nb_total_outputs + nb_outputs(nb_scope);
			elseif obj.gui == "CSCOPE" then
				scs_m.objs(i).model.rpar(4) = tf; //plus utilisé
			elseif obj.gui == "CMSCOPE" then
				scs_m.objs(i).model.rpar(2) = tf; //plus utilisé
			end;
		end;
	end;

    // Update TF
    scs_m.props.tf = tf;
endfunction;
