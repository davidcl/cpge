// ============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Bruno JOFRET
//
//  This file is distributed under the same license as the Scilab package.
// ============================================================================
//
// <-- ENGLISH IMPOSED -->
//
// <-- Short Description -->
// REP_TEMP

loadXcosLibs();

// Check where REP_TEMP block is defined
[a, b] = libraryinfo(whereis("REP_TEMP"));
assert_checkequal(grep(b, "cpge"), 1);

funcprot(0);
needcompile = 0;
alreadyran = %f;
%scicos_context = struct();

[status, message] = xcosValidateBlockSet("REP_TEMP");
if status == %f
    disp(message);
end
assert_checktrue(status);

disp(REP_TEMP("define"));
