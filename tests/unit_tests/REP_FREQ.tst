// ============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Bruno JOFRET
//
//  This file is distributed under the same license as the Scilab package.
// ============================================================================
//
// <-- ENGLISH IMPOSED -->
//
// <-- Short Description -->
// REP_FREQ

loadXcosLibs();

// Check where REP_FREQ block is defined
[a, b] = libraryinfo(whereis("REP_FREQ"));
assert_checkequal(grep(b, "cpge"), 1);

funcprot(0);
needcompile = 0;
alreadyran = %f;
%scicos_context = struct();

[status, message] = xcosValidateBlockSet("REP_FREQ");
if status == %f
    disp(message);
end
assert_checktrue(status);

disp(REP_FREQ("define"));
