// ============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Bruno JOFRET
//
//  This file is distributed under the same license as the Scilab package.
// ============================================================================
//
// <-- ENGLISH IMPOSED -->
//
// <-- Short Description -->
// CONST_m

loadXcosLibs();

// Check where CONST_m block is defined
[a, b] = libraryinfo(whereis("CONST_m"));
assert_checkequal(grep(b, "SCI"), 1);
assert_checkequal(grep(b, "scicos_blocks"), 1);
assert_checkequal(grep(b, "Sources"), 1);

funcprot(0);
needcompile = 0;
alreadyran = %f;
%scicos_context = struct();

[status, message] = xcosValidateBlockSet("CONST_m");
if status == %f
    disp(message);
end
assert_checktrue(status);

disp(CONST_m("define"))