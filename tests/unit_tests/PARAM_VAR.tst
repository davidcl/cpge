// ============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Bruno JOFRET
//
//  This file is distributed under the same license as the Scilab package.
// ============================================================================
//
// <-- ENGLISH IMPOSED -->
//
// <-- NOT FIXED -->
//
// <-- Short Description -->
// PARAM_VAR

loadXcosLibs();

// Check where CLR block is defined
[a, b] = libraryinfo(whereis("PARAM_VAR"));
assert_checkequal(grep(b, "cpge"), 1);

funcprot(0);
needcompile = 0;
alreadyran = %f;
%scicos_context = struct();

[status, message] = xcosValidateBlockSet("PARAM_VAR");
if status == %f
    disp(message);
end
assert_checktrue(status);
